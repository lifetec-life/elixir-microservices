defmodule Matchpool.Users.Entity.User do
  @moduledoc """
  Schema for matchpool users
"""
  use Ecto.Schema
  import Ecto.Changeset
  alias Matchpool.Users.Entity.{User, Profile}
  alias Matchpool.Users.Token


  @required_fields ~w(email password nickname)a
  @optional_fields ~w(password_reset_token activation_token)a

  @mail_regex ~r/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/


  @derive {Jason.Encoder, only: @required_fields ++ @optional_fields}
  schema "users" do
    field :email, :string
    field :password, :string
    field :nickname, :string
    field :activation_token, :string# JWT token for user activation
    field :password_reset_token, :string
    has_one :profile, Profile

    timestamps(inserted_at: :created_at)
  end

  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> put_password_hash()
    |> put_activation_token
    |> validate_required(@required_fields)
    |> unique_constraint(:email)
    |> unique_constraint(:nickname)
    |> validate_format(:email, @mail_regex)
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}}
      ->
        put_change(changeset, :password, Comeonin.Bcrypt.hashpwsalt(pass))
      _ ->
        changeset
    end
  end

  defp put_activation_token(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{email: em}}
      -> put_change(changeset, :activation_token, Token.generate_and_sign!(%{"email" => em }))
      _
      -> changeset
    end
  end

end
