defmodule Matchpool.Users.Entity.Profile do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset
  alias Matchpool.Users.Entity.User
  alias Matchpool.Users.Entity.Profile

  @required_fields ~w(first_name last_name birth_date user_id)a
  @optional_fields ~w(gender)a

  @derive {Jason.Encoder, only: @required_fields ++ @optional_fields}
  schema "user_profile" do
    field :first_name, :string
    field :last_name, :string
    field :birth_date, :naive_datetime
    field :gender, :integer, default: 0
    belongs_to(:user, User, foreign_key: :user_id)
    timestamps()
  end

  def changeset(%Profile{} = profile, attrs) do
    profile
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> assoc_constraint(:user)
    |> unique_constraint(:user_id)
  end
end
