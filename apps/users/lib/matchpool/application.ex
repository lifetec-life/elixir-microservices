defmodule Matchpool.Users.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false
  require Logger
  use Application

  def start(_type, _args) do
    Logger.info "Starting users service"
    {:ok, pid} = :net_kernel.start([:users])
    Logger.info "#{inspect(pid)}"
    import Supervisor.Spec
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: Users.Worker.start_link(arg)
      # {Users.Worker, arg}
      Matchpool.Users.Repo,
      supervisor(Task.Supervisor,
        [[name: Matchpool.Users.Task.Supervisor]],[restart: :temporary, shutdown: 1000])
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Users.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
