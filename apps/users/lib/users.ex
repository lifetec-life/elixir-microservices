defmodule Users do
  @moduledoc """
  Users service.
  """
  alias Matchpool.Users.Entity
  alias Matchpool.Users.Repo

  import Ecto.Query

  @doc """
  Creates new user.
  """
  def create_user(changeset) do
    %Entity.User{}
    |> Entity.User.changeset(changeset)
    |> Repo.insert()
  end

  def authenticate(email, password) do
    get_by_email(email)
    |> check_password(password)
  end

  def get_by_email(email) when is_binary(email) do
    Entity.User
    |> Repo.get_by(email: email)
  end

  def get_by_id(id) do
    user =  Repo.preload(Repo.get(Entity.User, id), :profile)
    if user == nil do
      {:error, :not_found}
    else
      user
    end
  end


  defp check_password(user, password) do
    case Comeonin.Bcrypt.checkpw(password, user.password) do
      true ->
          {:ok, user}
      false ->
         {:error}
    end
  end
end

defmodule Users.Supervisor do
  use Supervisor

  def start_link() do
    Supervisor.start_link(__MODULE__, :ok, [])
  end

  def init(:ok) do
    children = [
      Users
    ]
    Supervisor.init(children, strategy: :one_for_one)
  end

end
