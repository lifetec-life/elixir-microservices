defmodule Matchpool.Users.Repo.Migrations.UsersModifyTokenFields do
  use Ecto.Migration

  def change do
    alter table("users") do
      modify :activation_token, :text
      modify :password_reset_token, :text
    end
  end
end
