defmodule Matchpool.Users.Repo.Migrations.CreateUsers do
  use Ecto.Migration
  import Ecto.Changeset

  def change do
    create table(:users) do
      add :email, :string, null: false
      add :password, :string, null: false
      add :nickname, :string, null: false
      add :activation_token, :string, null: false # JWT token for user activation
      add :password_reset_token, :string, default: ""
      timestamps(inserted_at: :created_at)
    end

    create table(:user_profile) do
      add :first_name, :string
      add :last_name, :string
      add :birth_date, :naive_datetime
      add :gender, :int, default: 0
      add :user_id, references(:users)

      timestamps()
    end
  end
end
