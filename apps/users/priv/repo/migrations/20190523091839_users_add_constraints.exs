defmodule Matchpool.Users.Repo.Migrations.UsersAddConstraints do
  use Ecto.Migration

  def change do
    create unique_index(:users, [:email])
    create unique_index(:users, [:nickname])
    create unique_index(:user_profile, [:user_id])
  end
end
