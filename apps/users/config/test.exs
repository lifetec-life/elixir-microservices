use Mix.Config

config :users, Matchpool.Users.Repo,
       database: "matchpool_elixir_users_tests",
       username: "postgres",
       password: "postgres",
       hostname: "localhost",
       pool: Ecto.Adapters.SQL.Sandbox

config :users, ecto_repos: [Matchpool.Users.Repo]

config :joken, default_signer: "secret"