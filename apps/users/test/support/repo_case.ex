defmodule Matchpool.RepoCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      alias Matchpool.Users.Repo

      import Ecto
      import Ecto.Query
      import Matchpool.RepoCase

      # and any other stuff
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Matchpool.Users.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Matchpool.Users.Repo, {:shared, self()})
    end

    :ok
  end

end