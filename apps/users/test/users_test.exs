defmodule UsersTest do
  use Matchpool.RepoCase
  alias Matchpool.Users.Entity.User

  @user %{
    email: "test@test.com",
    password: "password",
    nickname: "mikeoz",
    activation_token: "sdfsdfasdfadsfs"
  }

  test "try to create user without required fields" do
    assert {:error, %Ecto.Changeset{}} = Users.create_user(%{@user | email: nil})
    assert {:error, %Ecto.Changeset{}} = Users.create_user(%{@user | password: nil})
    assert {:error, %Ecto.Changeset{}} = Users.create_user(%{@user | nickname: nil})
    assert {:error, %Ecto.Changeset{}} = Users.create_user(%{@user | email: "test@com"})
    assert {:error, %Ecto.Changeset{}} = Users.create_user(%{@user | email: "te.stcom.com"})
  end

  test "try to create users with duplicated fields" do
    assert {:ok, %User{}} = Users.create_user(@user)
    assert {:error, %Ecto.Changeset{}} = Users.create_user(%{@user | email: "doodle@test.com"})
    assert {:error, %Ecto.Changeset{}} = Users.create_user(%{@user | nickname: "newone"})
  end

  test "check password is hashed" do
    {:ok, user} = Users.create_user(@user)
    assert Comeonin.Bcrypt.checkpw(@user.password, user.password)
  end

  test "authorize user" do
    {:ok, %User{}} = Users.create_user(@user)
    assert {:ok, %User{}} = Users.authenticate(@user.email, @user.password)
  end

  test "check activation token exists for new user" do
    {:ok, %User{} = user} = Users.create_user(@user)
    assert user.activation_token != nil
    assert {:ok, token} = Matchpool.Users.Token.verify_and_validate(user.activation_token)
  end

  test "get user by email" do
    {:ok, %User{} = user} = Users.create_user(@user)
    user = Users.get_by_email(@user.email)
    assert user.email == @user.email
  end

  test "get user by id" do
    {:ok, %User{} = user} = Users.create_user(@user)
    new_user = Users.get_by_id(user.id)
    assert user.email == new_user.email
    assert user.id == new_user.id
  end

  test "test not found get user by id" do
    assert {:error, reason} = Users.get_by_id(10)
    assert reason == :not_found
  end
end

