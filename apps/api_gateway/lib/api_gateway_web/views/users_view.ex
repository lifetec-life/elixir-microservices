defmodule ApiGatewayWeb.UsersView do
  use ApiGatewayWeb, :view
  alias Matchpool.Users.Entity.User

  def render("user.json", %{user: user}) do
    %{
      email: user.email,
      id: user.id,
      nickname: user.nickname
    }
  end

  def render("user_changeset.json", %{changeset: changeset}) do
    %{errors: Ecto.Changeset.traverse_errors(changeset, fn {msg, _opts} -> msg end)}
  end

  def render("user_changeset_error.json", %{error: error}) do
    %{}
  end
end