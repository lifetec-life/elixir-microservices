defmodule ApiGatewayWeb.PageView do
  use ApiGatewayWeb, :view
  alias Matchpool.Users.Entity.User

  def render("user.json", %{user: user}) do
    %{
      email: user.email,
      id: user.id,
      nickname: user.nickname
    }
  end
end
