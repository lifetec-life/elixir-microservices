defmodule ApiGatewayWeb.UsersController do
  use ApiGatewayWeb, :controller
  alias ApiGatewayWeb.ErrorView
  alias Matchpool.Users.Entity.User
  alias Matchpool.Users.Token
  require Logger

  plug ApiGateway.Guardian

  def index(conn, _params) do

  end

  def show(conn, %{"id" => id}) do
    case user = UsersInterface.get_by_id(id) do
      %User{} ->
        conn
        |> assign(:user, user)
        |> render("user.json")
      {:error, reason} ->
        conn
        |> put_status(reason)
        |> put_view(ErrorView)
        |> render(:"404")
    end
  end

  def create(conn, params) do
    case UsersInterface.create_user(params) do
      {:ok, user = %User{}} ->
        conn
        |> assign(:user, user)
        |> render("user.json")
      {:error, change = %Ecto.Changeset{}} ->
        conn
        |> assign(:changeset, change)
        |> render("user_changeset.json")
    end
    json(conn, "")
  end

  def update(conn, params = %{"id"=>id}) do
    json(conn, params)
  end

  def delete(conn, _params) do

  end

  def authenticate(conn, params) do
    case UsersInterface.authenticate(Map.get(params,"email"), Map.get(params,"password")) do
      {:ok, user} ->
        json(conn, Token.generate_and_sign!(%{"email" => user.email }))
    end
    json(conn, %{})
  end
end