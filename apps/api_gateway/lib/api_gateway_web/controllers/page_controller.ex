defmodule ApiGatewayWeb.PageController do
  use ApiGatewayWeb, :controller
  alias Matchpool.Users.Entity.User
  require Logger

  def index(conn, _params) do
    Logger.info Node.self()
    Logger.info UsersInterface.create_user(%User{})
    render(conn, "index.html")
  end
end
