defmodule ApiGatewayWeb.Router do
  use ApiGatewayWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ApiGatewayWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api", ApiGatewayWeb do
    pipe_through :api
    resources "/users", UsersController, except: [:new, :edit]
    post "/authenticate", UsersController, :authenticate
  end

  # Other scopes may use custom stacks.
  # scope "/api", ApiGatewayWeb do
  #   pipe_through :api
  # end
end
