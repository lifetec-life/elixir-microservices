defmodule ApiGateway.Guardian do
  import Plug.Conn
  require Logger

  def init(default), do: default

  def call(conn, _default) do
    case get_token(conn) do
      {:not_found} ->
        deny(conn)
      {:ok, token} ->
        case authorize_token(token) do
          {:error} -> deny(conn)
          {:authorized} -> conn
        end
    end
  end

  defp get_token(conn) do
    case get_req_header(conn, "authorization") do
      [] -> {:not_found}
      [header] ->
        {:ok, String.replace(header,"Bearer ", "")}
    end
  end

  defp authorize_token(token) do
    case Matchpool.Users.Token.verify_and_validate(token) do
      {:ok, tok} ->
        case UsersInterface.get_by_email(Map.get(tok, "email")) do
          user -> {:authorized}
          {:error, reason} -> {:error}
        end
      {:error, reason} -> {:error}
    end
  end

  defp deny(conn) do
    send_resp(conn, 401, "{}")
    Phoenix.Controller.render(conn, ApiGatewayWeb.ErrorView, "error.json")
    Plug.Conn.halt(conn)
  end
end