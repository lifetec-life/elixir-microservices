defmodule ApiGateway.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false
  require Logger
  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    :net_kernel.start([:"api@LAPTOP-6F54B1RA"])
    :erl_boot_server.start([])
    Logger.info :net_kernel.connect_node(:"users@LAPTOP-6F54B1RA")

    children = [
      # Start the endpoint when the application starts
      ApiGatewayWeb.Endpoint
      # Starts a worker by calling: ApiGateway.Worker.start_link(arg)
      # {ApiGateway.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ApiGateway.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    ApiGatewayWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
