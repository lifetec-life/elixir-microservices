defmodule UsersInterface do
  use GenServer

  def spawn_link(opts)do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  def handle_call({:call, module, fun, args}) when is_binary(module) do
    module
    |> String.capitalize(module)
    |> Kernel.<> "Elixir."
    |> spawn_task(fun, args)
  end

  def spawn_task(module, fun, args, env \\ Mix.env) do
    do_spawn_task({module, fun, args}, env)
  end

  defp do_spawn_task({module, fun, args}, :test) do
    apply(module, fun, args)
  end

  defp do_spawn_task({module, fun, args}, _) do
    Task.Supervisor.async(remote_supervisor(), module, fun, args)
    |> Task.await
  end

  defp remote_supervisor do
    {
      Application.get_env(:users_interface, :task_supervisor),
      Application.get_env(:users_interface, :node)
    }
  end


  def create_user(user) do
    spawn_task(Users, :create_user, [user])
  end

  def get_by_email(email) when is_binary(email) do
    spawn_task(Users, :get_by_email, [email])
  end

  def get_by_id(id) do
    spawn_task(Users, :get_by_id, [id])
  end

  def authenticate(email, password) do
    spawn_task(Users, :authenticate, [email, password])
  end
end
