use Mix.Config
config :phoenix, serve_endpoints: true

config :users_interface,
       task_supervisor: Matchpool.Users.Task.Supervisor,
       node: :"users@LAPTOP-6F54B1RA"
import_config "../apps/*/config/config.exs"